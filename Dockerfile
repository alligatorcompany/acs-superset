FROM python:3.8-slim-buster

RUN apt update >/dev/null && apt -qqy install --no-install-recommends \
    build-essential libssl-dev libffi-dev libkeyutils-dev \
    python-dev python-pip libsasl2-dev libldap2-dev supervisor \
    unixodbc-dev unixodbc-bin unixodbc ca-certificates \
    >/dev/null 

RUN python -m pip install --upgrade pip >/dev/null
RUN pip install sqlalchemy==1.3.23 apache-superset==1.0.1 >/dev/null

RUN superset db upgrade >/dev/null 2>&1 && \
    superset fab create-admin --username admin \
    --firstname superset --lastname admin --email admin@example.com --password admin >/dev/null 2>&1 && \
    superset init >/dev/null 2>&1

RUN pip install pyodbc==4.0.30 sqlalchemy-exasol==2.2.0 >/dev/null
ADD https://www.exasol.com/support/secure/attachment/111515/EXASOL_ODBC-7.0.0.tar.gz /
RUN tar xfz /EXASOL_ODBC*.tar.gz && rm /EXASOL*.tar.gz && chown -R root:root /EXA*/*
COPY exasol_odbcinst.ini /etc/odbcinst.ini

EXPOSE 8088

COPY supervisord /etc/supervisor/conf.d/supervisord.conf
CMD ["/usr/bin/supervisord"]
